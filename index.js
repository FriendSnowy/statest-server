const express = require('express')
const childProcess = require('child_process');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const app = express()
const port = 100;

class JSON_OBJECT {
    constructor() {
        this.json = {};
    }

    put(key, value) {
        this.json[key] = value;
        return this;
    }

    toAnswer(status = true) {
        this.put('response', status);
        return JSON.stringify(this.json);
    }
    stringify() {
        return JSON.stringify(this.json);
    }
}

app.use(fileUpload());

app.get('/', (request, response) => {
    response.send('Hello');
});
app.get('/test', async (request, response) => {
    response.setHeader('Content-Type', 'application/json');
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.send(await testHandler(request, response));
});
app.get('/testList', async (request, response) => {
    response.setHeader('Content-Type', 'application/json');
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.send(await testListHandler(request, response));
});
app.post('/testUpload', async (request, response) => {
    response.setHeader('Content-Type', 'application/json');
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.send(await testUploadHandler(request, response));
})
app.listen(port, (err) => {
    if (err) 
        return console.log('something bad');
    console.log('ok');
});

const empty = param => {
    if (param === undefined || param === null)
        return true;
    else
        return false;
}

const testHandler = async (request = null, response = null) => {
    /*
        На вход - имя теста (testName)
        На выходе - результат выполнения
    */

    const jsonAnswer = new JSON_OBJECT;
    if (empty(request.query.testName)) {
        jsonAnswer.put('error', 'Params is not founded')
        return jsonAnswer.toAnswer(false);
    }

    let testName = request.query.testName;

    if (!fs.existsSync(`./tests/${testName}.js`) && !fs.existsSync(`./tests/${testName}`)) {
        return jsonAnswer.put('error', 'Test file is not founded').put('errorCode', '0').toAnswer(false);
    }

    let testResult = await require(`./tests/${testName}`).test.Execute();
    delete require.cache[require.resolve(`./tests/${testName}`)];

    return jsonAnswer.put('result', testResult).toAnswer();
}

const testListHandler = async (request = null, response = null) => {
    /* 
        На вход - ничего
        На выходе - список тестов
    */
    const jsonAnswer = new JSON_OBJECT;

    if (!fs.existsSync('./testInfo.json'))
        return jsonAnswer.put('tests', {}).toAnswer(false);

    
    let json = require('./testInfo.json');
    delete require.cache[require.resolve('./testInfo.json')];

    if (empty(request.query.testName)) {
        let finalJSON = {};
        Object.keys(json.tests).forEach((key) => {
            finalJSON[key] = json.tests[key];
        });

        return jsonAnswer.put('tests', finalJSON).toAnswer();
    }
    else {
        let testName = request.query.testName;
        if (empty(json.tests[testName]))
            return jsonAnswer.put('tests', {}).toAnswer(false);
        else
            return jsonAnswer.put('test', json.tests[testName]).toAnswer();
    }
}

const testUploadHandler = async (request = null, response = null) => {
    /*
        Структура json-а testinfo:
        "tests": {
            "test-test" : {
                "filename":"test-test.js",
                "name":"Количество ссылок больше 3",
                "description":"Проверяет чтобы количество особых ссылок было больше трех"
            }
        }
    */
    const jsonAnswer = new JSON_OBJECT;

    let testName = request.param('testName', undefined);
    let testDescription = request.param('testDescription', undefined);

    if (empty(request.files.testFile) || empty (testName) || empty(testDescription)) {
        jsonAnswer.put('error', 'Not all params founded')
        return jsonAnswer.toAnswer(false);
    }

    let testFile = request.files.testFile;

    testFile.mv(`./tests/${testName}.js`, (err) => {
        if (err)
            return jsonAnswer.put('error', 'File moving error').put('errorCode', '1').toAnswer(false);
    });

    let testJson = {};

    if (fs.existsSync('./testInfo.json')) {
        testJson = require('./testInfo.json');
        delete require.cache[require.resolve('./testInfo.json')];
    }
    else {
        testJson["tests"] = {};
    }

    testJson["tests"][testName] = {
        "filename" : `${testName}.js`,
        "name" : `${testName}`,
        "description" : `${testDescription}`
    };

    fs.writeFileSync('./testInfo.json', JSON.stringify(testJson), err => {
        return jsonAnswer.put('error', 'JSON creation error').put('errorCode', '2').toAnswer(false);
    })
    
    return jsonAnswer.toAnswer(true);
}